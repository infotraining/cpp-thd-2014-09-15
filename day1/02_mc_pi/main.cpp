#include <iostream>
#include <random>
#include <chrono>
#include <thread>
#include <algorithm>

using namespace std;

double calc_hit_ratio(long N, long& counter)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);

    for (int n = 0; n < N; ++n) {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            counter++;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    auto start = chrono::high_resolution_clock::now();
    long N = 10000000;
    long counter = 0;
    calc_hit_ratio(N, counter);
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed single thread: " <<
    chrono::duration_cast<chrono::milliseconds>(end-start).count()
    << " ms" << endl;
    cout << "Pi = " << (double(counter)/(double)N)*4 << endl;

    /* -- threads -- */

    vector<thread> threads;
    int hwc = 4; //thread::hardware_concurrency();
    vector<long> counters(hwc);

    start = chrono::high_resolution_clock::now();
    for (int i = 0 ; i < hwc ; ++i)
        threads.emplace_back(calc_hit_ratio, N/hwc, ref(counters[i]));

    for(auto& th : threads) th.join();
    long hits = accumulate(counters.begin(), counters.end(), 0l);

    end = chrono::high_resolution_clock::now();

    cout << "Elapsed multi thread: " <<
    chrono::duration_cast<chrono::milliseconds>(end-start).count()
    << " ms" << endl;

    cout << "Pi = " << (double(hits)/(double)N)*4 << endl;

    return 0;
}

