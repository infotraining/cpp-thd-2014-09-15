#include <iostream>
#include <thread>
#include <vector>

// leszek.tarkowski@infotraining.pl

using namespace std;

void task(int& id)
{
    cout << "From task " << id << endl;
}

void manytask(int id)
{
    cout << "From manytask " << id << endl;
}

thread start_thread()
{
    int val = 10;
    thread tmp(task, ref(val));
    return tmp;
}

int main()
{
    cout << "Hello World!" << endl;
    vector<thread> threads;

    threads.emplace_back(&manytask, 1);
    for (int i = 2 ; i < 10 ; ++i )
        threads.emplace_back(&manytask, i);

    //thread th = start_thread();
    //th.join();
    for (thread& th : threads) th.join();
    return 0;
}

