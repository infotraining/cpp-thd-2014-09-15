#include <iostream>
#include <thread>

using namespace std;

void task(int id)
{
    cout << "Hello from thread " << id << endl;
}

void increment(int &a)
{
    a++;
}

struct Test
{
    void operator()()
    {
        cout << "Hello from Test functor" << endl;
    }
};

int main()
{
    cout << "Hello World!" << endl;

    thread th1(&task, 10);
    int cos = 5;
    int cos2 = 1;

    thread th2( [&cos] () { cout << "Hello from lambda " << cos++ << endl;});

    thread th3(&increment, ref(cos2));

    Test t;
    thread th4(t);

    th1.join();
    th2.join();
    th3.join();
    th4.join();
    cout << "cos: " << cos << endl;
    cout << "cos2: " << cos2 << endl;
    return 0;
}

