#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;

long counter = 0;
atomic<long> at_counter(0);
mutex mtx;

void increase()
{
    for(int i = 0 ; i < 10000 ; ++i)
    {
        mtx.lock();
        ++counter;
        mtx.unlock();
    }
}

void at_increase()
{
    for(int i = 0 ; i < 10000 ; ++i)        
        at_counter.fetch_add(1, memory_order_relaxed);
}

int main()
{
    cout << "Hello World!" << endl;
    vector<thread> thds;

    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 8 ; ++i)
        thds.emplace_back(at_increase);

    for (auto& th : thds) th.join();

    auto end = chrono::high_resolution_clock::now();
    cout << "Counter - should be 80000: " << at_counter << endl;
    cout << "Elapsed atomic: " <<
    chrono::duration_cast<chrono::microseconds>(end-start).count()
    << " us" << endl;

    vector<thread> thds2;

    start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 8 ; ++i)
        thds2.emplace_back(increase);

    for (auto& th : thds2) th.join();

    end = chrono::high_resolution_clock::now();
    cout << "Counter - should be 80000: " << counter << endl;
    cout << "Elapsed not safe: " <<
    chrono::duration_cast<chrono::microseconds>(end-start).count()
    << " us" << endl;

    return 0;
}

