#include <iostream>
#include <mutex>
#include <thread>

using namespace std;

class BankAccount
{
    int id_;
    double balance_;
    mutex mtx_;
public:
    BankAccount(int id, double balance) :
        id_(id), balance_(balance)
    {
    }

    void print()
    {
        cout << "BA: " << id_;
        cout << " balance = " << balance_ << endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        unique_lock<mutex> l1(mtx_, defer_lock);
        unique_lock<mutex> l2(to.mtx_, defer_lock);
        lock(l1, l2);
        balance_ -= amount;
        to.balance_ += amount;
    }
};

void test(BankAccount& from, BankAccount& to)
{
    for (int i = 0 ; i < 100000 ; ++i)
    {
        from.transfer(to, 1.0);
    }
}

int main()
{
    cout << "Hello World!" << endl;
    BankAccount ac1(1, 100000);
    BankAccount ac2(2, 100000);
    thread th1(test, ref(ac1), ref(ac2));
    thread th2(test, ref(ac2), ref(ac1));
    th1.join();
    th2.join();
    ac1.print();
    ac2.print();
    return 0;
}

