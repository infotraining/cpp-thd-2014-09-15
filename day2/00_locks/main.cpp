#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;

long counter = 0;
mutex mtx;

class Locker
{
    mutex & mtx_;
public:
    Locker(mutex& mtx) : mtx_(mtx)
    {
        mtx_.lock();
    }

    ~Locker()
    {
        mtx_.unlock();
    }
};

void increase()
{
    for(int i = 0 ; i < 10000 ; ++i)
    {
        lock_guard<mutex> l(mtx);
        ++counter;
        if (counter == 1000) return;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    vector<thread> thds;

    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < 8 ; ++i)
        thds.emplace_back(increase);

    for (auto& th : thds) th.join();

    auto end = chrono::high_resolution_clock::now();
    cout << "Counter - should be 80000: " << counter << endl;
    cout << "Elapsed: " <<
    chrono::duration_cast<chrono::microseconds>(end-start).count()
    << " us" << endl;

    return 0;
}

