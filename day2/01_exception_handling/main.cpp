#include <iostream>
#include <thread>
#include <stdexcept>
#include <exception>
#include <string>

using namespace std;

void may_throw(int id, int arg)
{
    if (arg == 13)
        throw runtime_error("Error 42 from " + to_string(id));
}

void worker(int id, exception_ptr& e)
{
    try
    {
        for (int i = 0 ; i < 50 ; ++i)
        {
            cout << "THD " << id << " : " << i << endl;
            this_thread::sleep_for(200ms);
            may_throw(id, i);
        }
    }
    catch(...)
    {
        cout << "Catch in " << id << endl;
        e = current_exception();
    }
}

int main()
{
    cout << "Hello World!" << endl;
    exception_ptr ex1;
    exception_ptr ex2;
    thread th1(&worker, 1, ref(ex1));
    thread th2(&worker, 2, ref(ex2));
    th1.join();
    th2.join();
    if (ex1)
    {
        try
        {
            rethrow_exception(ex1);
        }
        catch(const runtime_error& e)
        {
            cout << "There was exception in th 1: ";
            cout << e.what() << endl;
        }
    }
    return 0;
}

