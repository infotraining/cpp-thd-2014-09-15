#include <iostream>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex qmtx;
condition_variable cond;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        this_thread::sleep_for(200ms);
        {
            lock_guard<mutex> lg(qmtx);
            q.push(i);
            cond.notify_one();
            cout << "produced " << i << endl;
        }
    }
}

void consumer(int id)
{
    for(;;)
    {
        {
            unique_lock<mutex> lg(qmtx);
//            while(q.empty())
//            {
//                cond.wait(lg);
//            }
            cond.wait(lg, [](){return !q.empty();});
            int msg = q.front();
            q.pop();
            cout << "cons " << id << " got " << msg << endl;
        }
    }
}

int main()
{
    cout << "Hello World!" << endl;
    vector<thread> thrds;
    thrds.emplace_back(producer);
    thrds.emplace_back(consumer, 1);
    thrds.emplace_back(consumer, 2);
    thrds.emplace_back(consumer, 3);
    thrds.emplace_back(consumer, 4);
    for(auto& th : thrds) th.join();
    return 0;
}

