#include <iostream>
#include <vector>
#include <thread>
#include "thread_safe_queue.h"


using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        q.push(i);
        cout << "produced " << i << endl;
    }
}

void consumer(int id)
{
    for(;;)
    {
        int msg;
        q.pop(msg);
        cout << "cons " << id << " got " << msg << endl;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    vector<thread> thrds;
    thrds.emplace_back(producer);
    thrds.emplace_back(consumer, 1);
    thrds.emplace_back(consumer, 2);
    thrds.emplace_back(consumer, 3);
    thrds.emplace_back(consumer, 4);
    for(auto& th : thrds) th.join();
    return 0;
}
