#include <iostream>
#include <thread>
#include <future>

using namespace std;

int answer()
{
    this_thread::sleep_for(1s);
    return 42;
}

int main()
{
    cout << "Hello World!" << endl;
    future<int> res1 = async(launch::async, answer);

    packaged_task<int()> pt(answer);
    future<int> res2 = pt.get_future();
    thread th(move(pt));
    th.detach();

    cout << "Result is: " << res1.get() << endl;
    cout << "Result is: " << res2.get() << endl;
    return 0;
}

