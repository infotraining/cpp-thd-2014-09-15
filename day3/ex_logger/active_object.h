#ifndef ACTIVE_OBJECT_H
#define ACTIVE_OBJECT_

#include <functional>
#include <thread>
#include "../../day2/04_TSQ/thread_safe_queue.h"

typedef std::function<void()> t_task;

class active_object
{
    thread_safe_queue<t_task> q;
    std::thread servant;
public:
    active_object()
    {
        servant = std::thread( [this] () {
            for(;;)
            {
                t_task t;
                q.pop(t);
                if (t)
                    t();
                else
                    return;
            }
        });
    }

    void send(t_task task)
    {
        q.push(task);
    }

    ~active_object()
    {
        q.push(nullptr);
        servant.join();
    }
};

#endif // ACTIVE_OBJECT_H
