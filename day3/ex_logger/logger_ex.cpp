#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
#include <chrono>
#include <functional>
#include <boost/lexical_cast.hpp>
#include "active_object.h"

using namespace std;

namespace Before
{

class Logger
{
    ofstream fout_;
    active_object ao;

public:
    Logger(const string& file_name)
    {
        fout_.open(file_name);
    }

    ~Logger()
    {
        //fout_.close();
    }

    void log(const string& message)
    {
        ao.send( [this, message] { log_(message);});
    }

private:
    void log_(const string& message)
    {
        this_thread::sleep_for(100us);
        fout_ << message << endl;
        fout_.flush();
    }
};

}

using namespace Before;

void run(Logger& logger, int id)
{
    for(int i = 0; i < 1000; ++i)
       logger.log("Log#" + boost::lexical_cast<string>(id) + " - Event#" + boost::lexical_cast<string>(i));
}

int main()
{
    /*
     * Napisz klase Logger, ktora jest thread-safe
     */

    Logger log("data.log");

    thread thd1(&run, ref(log), 1);
    thread thd2(&run, ref(log), 2);

    thd1.join();
    thd2.join();
}
