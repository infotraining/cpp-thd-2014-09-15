#include <iostream>
#include <boost/filesystem.hpp>
#include <vector>
#include "../../day2/04_TSQ/thread_safe_queue.h"

using namespace std;
thread_safe_queue<boost::filesystem::path> inp_q;
thread_safe_queue<long> out_q;
int N = 2;

void producer(string start_path, string filter)
{
    boost::filesystem::recursive_directory_iterator it(start_path);
    boost::filesystem::recursive_directory_iterator end;

    for ( ; it != end ; ++it)
    {
        if(boost::filesystem::is_regular_file(*it))
        {
            if(boost::filesystem::extension(*it) == filter)
            {
                inp_q.push(*it);
            }
        }
    }
    inp_q.push(boost::filesystem::path());
}

void worker()
{
    for(;;)
    {
        boost::filesystem::path file;
        inp_q.pop(file);
        if (!file.empty())
            out_q.push(boost::filesystem::file_size(file));
        else
        {
            inp_q.push(file);
            out_q.push(-1);
            return;
        }
    }
}

void reductor()
{
    long total_size = 0;
    int finished = 0;
    for(;;)
    {
        long size;
        out_q.pop(size);
        if (size == -1)
        {
            finished++;
            if (finished == N)
            {
                cout << total_size << endl;
                return;
            }
        }
        total_size += size;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    vector<thread> threads;
    threads.emplace_back(producer, "/home/leszek/_code_", ".cpp");
    threads.emplace_back(reductor);
    for(int i = 0 ; i < N ; ++i)
    {
        threads.emplace_back(worker);
    }

    for (auto& th : threads) th.join();
    return 0;
}

