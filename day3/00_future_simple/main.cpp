#include <iostream>
#include <thread>
#include <future>

using namespace std;

int answer()
{
    this_thread::sleep_for(5s);
    return 42;
}

int main()
{
    cout << "Hello World!" << endl;
    future<int> res1 = async(launch::async, answer);
    future<int> res2 = async(launch::async, answer);
    cout << "Result is: " << res1.get() << endl;
    cout << "Result is: " << res2.get() << endl;
    return 0;
}

