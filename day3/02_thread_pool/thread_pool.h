#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include "../../day2/04_TSQ/thread_safe_queue.h"
#include <functional>
#include <vector>
#include <thread>
#include <future>

typedef std::function<void()> t_task;

class thread_pool
{
    thread_safe_queue<t_task> q_;
    std::vector<std::thread> thds_;

    void worker()
    {
        for(;;)
        {
            t_task t;
            q_.pop(t);
            if (t)
                t();
            else return;
        }
    }

public:
    thread_pool(int size)
    {
        for (int i = 0 ; i < size ; ++i)
        {
            thds_.emplace_back(&thread_pool::worker, this);
        }
    }

    void add_task(t_task task)
    {
        if (task)
            q_.push(task);
    }

    template<typename F>
    auto async(F f)
    {
        auto task = std::make_shared<std::packaged_task<typename std::result_of<F()>::type()>>(f);
        auto res = task->get_future();
        q_.push( [task] () {
            (*task)();
        });
        return res;
    }

    ~thread_pool()
    {
        for(auto& th : thds_) q_.push(nullptr);
        for(auto& th : thds_) th.join();
    }
};

#endif // THREAD_POOL_H
