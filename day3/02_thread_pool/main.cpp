#include <iostream>
#include "thread_pool.h"

using namespace std;

void task()
{
    cout << "task for thread pool" << endl;
}

int answer()
{
    return 42;
}


int main()
{
    cout << "Hello World!" << endl;
    thread_pool tp(2);
    tp.add_task(&task);
    tp.add_task( [] () { cout << "lambda 2 in tp" << endl;});
    tp.add_task( [] () { cout << "lambda 4 in tp" << endl;});
    future<int> res = tp.async(answer);
    cout << "Answer: "  << res.get() << endl;
    return 0;
}

